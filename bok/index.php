<?php
	include_once 'Model/Books.php';
	include_once 'Model/Book.php';
	include_once 'Controller/Controller.php';
	//Instaniate Model og Controller
	$model = new Books();
	$controller = new Controller($model);
	//Býr til breytu books sem sækir í selectBook skipun í gegnum controllerinn
	$books=$controller->selectBook();

	//ef select html er settað, runna echo sem sækir bókaupplýsingar
	if(isset($_GET['book'])){
		echo "<b>Bókarnafn: </b>" . $books->nafn . " <b>Útgáfa: </b>" . $books->utgafa . " <b>Útgáfuár: </b>" . $books->utgafuar;
		exit;
	}

	include_once 'View/View.php';
?>