<?php

class Controller{

	//private property fyrir modelið
	private $model;

	public function __construct(Books $model){
		$this->model = $model;
	}

	//method notað til þess að sækja bók úr modelinu
	public function selectBook(){
		//ef ekkert er í book select html taginu, þá á að fylla það með getBooks method
		if (!isset($_GET['book'])) {
			return $this->model->getBooks();
		}
		//ef select er fyllt, return þá upplýsingum um sértstaka bók
		else{
			return $this->model->getBook ($_GET['book']);
		}
	}

}