<?php


class Book {
	//Nafn, útgáfa og útgáfuár
	public $nafn;
	public $utgafa;
	public $utgafuar;

	public function __construct($nafn, $utgafa, $utgafuar) {
		$this->nafn = $nafn;
		$this->utgafa = $utgafa;
		$this->utgafuar = $utgafuar;
	}

}