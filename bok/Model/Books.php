<?php

class Books{
	//getBooks býr til objecta úr book modelinu og setur í array og returnar
	public function getBooks() {
		$bakur = array(
			'To Kill A Mockingbird' => new book('To Kill A Mockingbird','1','1960'),
			'The Great Gatsby' => new book('The Great Gatsby','1','1925'),
			'Of Mice and Men' => new book('Of Mice and Men','1','1937'),
			'Nineteen Eighty-Four' => new book('Ninteen Eighty-Four','1','1949')
		);
		return $bakur;
	}

	//nær í einstaka bók úr getBooks method
	public function getBook ($nafn){
		$allbooks = $this->getBooks();
		return $allbooks[$nafn];
	}
}